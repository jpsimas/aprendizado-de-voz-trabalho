# -*- coding: utf-8 -*-
# -*- mode: org -*-

#+TITLE: PSI5123 - Lista 2
#+AUTHOR: João Pedro de Omena Simas

#+STARTUP: overview indent
#+LANGUAGE: en-us
#+OPTIONS: H:3 creator:nil timestamp:nil skip:nil toc:nil num:t ^:nil ~:~
#+OPTIONS: author:nil title:nil date:nil
#+TAGS: noexport(n) deprecated(d) ignore(i)
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport

#+LATEX_CLASS: IEEEtran
#+LATEX_CLASS_OPTIONS: [peerreview,letter,10pt,final]
#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER: \usepackage[T1]{fontenc}
#+LATEX_HEADER: \usepackage{svg}
#+LATEX_HEADER: \usepackage{placeins}

#+LANGUAGE: brazilian
#+LATEX_HEADER: \usepackage[brazilian]{babel}

* IEEETran configuration for org export + ignore tag (Start Here)  :noexport:

#+begin_src emacs-lisp :results output :session :exports both
  (add-to-list 'load-path ".")
  (require 'ox-extra)
  (ox-extras-activate '(ignore-headlines))
  (add-to-list 'org-latex-classes
               '("IEEEtran"
                 "\\documentclass{IEEEtran}"
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
  (add-to-list 'org-latex-packages-alist '("" "minted"))
  (setq org-latex-listings 'minted)
#+end_src

#+RESULTS:

* Notes :noexport:
  - Dois artigos
  - Banco de dados
    + tipo de arquivo
    + como são importados
    + treinamento
      - janela
      - batch
      - atributos
      - transformacoes
      - programas (aprendice)
  - etapas do projeto
    + diagrama de blocos
    + alterações e simplificações
  - medidas quantitativas
    + função custo, figuras de mérito e treinamento

* *The Paper*                                                       :ignore:
** Latex configurations                                             :ignore:
** Front Page                                                       :ignore:
#+BEGIN_EXPORT latex
\makeatletter
\let\orgtitle\@title
\makeatother

\title{\orgtitle}

\author{
\IEEEauthorblockN{
João Pedro de Omena Simas\IEEEauthorrefmark{1}}

\IEEEauthorblockA{\IEEEauthorrefmark{1}
Universidade de São Paulo, São Paulo, Brazil}
}
#+END_EXPORT

#+LaTeX: \maketitle

** Artigos e Referências
O principal artigo, aquele do qual a estrutura do classificador foi
retirado, é cite:graves2013. Este propõe uma estrutura de rede
profunda utilizando LSTMs bidirecionais aplicados à detecção fonética,
obtendo bons resultados com a base de dados TIMIT. Além disso,
cite:lopesPerdigao11 apresenta uma revisão da literatura extensa no
que se refere a soluções ao problema de detecção fonética na base de
dados TIMIT, servindo de base para várias decisões de projeto ao
decorrer de seu desenvolvimento, com base nas diversas soluções
previamente propostas para o problema apresentadas e as métricas
utilizadas para a avaliação de performance nesse problema em particular.
** Banco de Dados
O banco de dados utilizado é o banco de dados TIMIT cite:timit, que
contém trechos de fala de 10 frases ditas por 630 falantes de inglês
de 8 regiões dialetais distintas dos Estados Unidos, acompanhados de
transcrições fonéticas feitas em uma versão modificada do
código de transcição fonética ARPABET. Esses trechos de fala estão
presentes como arquivos .wav amostrados em 16 kHz.
Os arquivos são importados e subamostrados a 8 kHz utilizando a
biblioteca /librosa/ na linguagem /Python/. Estes são, então, codificados
com uma codificação LPC (linear predictive coding) em blocos de 10ms
(80 amostras) e os coeficientes dos preditores obtidos, juntamente com
a potência em curto prazo nestes mesmos blocos, são exportados para arquivos
binários a serem utilizados como atributos para treinamento.

As classes (/labels/), i.e. os fonos, são mantidas como estão no
dataset, com excessão das três classes que representam tipos de
silêncio ("h#", "pau" e "epi") que são todas substituídas por "h#". Em
seguida estas são codificadas numericamente na sequência que aparecem
no dataset. Finalmente, este sinal é subamostrado, atribuindo a cada
bloco o fono mais frequente neste. 

O treinamento é feito, até o presente momento, em /batches/ de 32
entradas, e cada entrada, uma sequência de 16 amostras, totalizando
512 amostras.A divisão em sequências se faz necessária, pois o modelo
sendo utilizado é recorrente (Bi-LSTM).

O código utilizado para realizar o pré-processamento pode ser
encontrado no anexo 1.
** Descrição do Sistema
A topologia de aprendizado escolhida para o classificador (que não estava
diretamente especificada na proposta inicial), foi de uma rede de
LSTMs bidirecionais com uma camada oculta como proposto em
cite:graves2013. Um diagrama geral do sistema, compreendendo a
codificação e classificação do sinal pode ser visto abaixo:
#+LATEX:\FloatBarrier
#+CAPTION: Diagrama de blocos geral do sistema de classificação.
[[../img/system.eps]]
#+LATEX:\FloatBarrier
A alteração introduzida são os atributos utilizados, que são os
coeficientes do preditor linear ótimo estimado bloco-a-bloco (LPC),
isto é o vetor de pesos w que minimiza:
\begin{equation}
\sum_{i = 0}^{M - N - 1}\left\lvert \left(\sum_{k = 0}^{N - 1} w_k x_{i + k}\right) - x_{i + N}\right\rvert^2
\end{equation}
onde M é o tamanho do bloco e N a dimensão do preditor,
e a potência de curto prazo, também calculada em blocos, ao invés de
MFCCs.
Como função custo de aprendizado, por se tratar de um classificador
multi-classe, foi utilizada a entropia cruzada e como otimizador, o
algorítimo Adam. A implementação foi feita utilizando a biblioteca
/pytorch/.
Adicionalmente, será implementado também o treinamento por
/connectionist temporal classification/ (CTC), como
proposto em cite:graves2013, entretanto, isso ainda não foi implementado.
** Medidas
As medidas utilizadas para avaliar o desempenho do classificador são,
primeiramente, a acurácia, por ser uma medida direta da performance do
classificador, a função custo de treinamento, no caso a entropia cruzada da
saída com a saída esperada, que captura também a performance do
classificador, mas capturando também a convergência do aprendizado de
maneira mais expressiva.

Para comparar com outros métodos em sistemas de reconhecimento de
fala, a taxa erro de fono é utilizada. Apresentada em
cite:lopesPerdigao11, é uma medida comumente utilizada para esse tipo de
sistema. Esta é definida pela distância de Levenshtein entre a
sequência de fonos esperada e aquela gerada pelo classificado, isto é:
\begin{equation}
PER = \frac{S + D + I}{N_T}
\end{equation}
Onde \(N_T\) é o número total de fonos na sequência esperada, S é o número de
substituições, D o de deleções e I de inserções de fonos na saída
predita. Esta é útil neste caso, pois, primeiramente, leva em
consideração os tipos de erro que comumente ocorrem nesse tipo de
sistema.

** References                                                        :ignore:
[[bibliographystyle:IEEEtran]]
[[bibliography:refs.bib]]

\pagebreak
** Anexo 1 - Código do Pré-Processamento

#+ATTR_LATEX: :float nil :options fontsize=\small, breaklines, breakautoindent=true
#+BEGIN_SRC python
  ## Código do pré processamento inicial (leitura, re-amostragem e codificação)
  import pandas as pd
  import numpy as np
  import librosa
  import soundfile as sf

  from matplotlib import pyplot as plt 

  from pathlib import Path
  import os

  import time

  from nlms_encode import nlms_encode

  def readTimit(fname, sample_rate = None, dic = {}):

      count = len(dic)

      data = pd.read_csv(fname + ".PHN", delimiter = " ", header = None, names = ["begin", "end", "phoneme"], dtype = {"begin" : int, "end" : int, "phoneme" : str})

      x, sr = librosa.load(fname + ".WAV", sample_rate)
      x = librosa.effects.preemphasis(x)

      u = np.zeros(x.shape, dtype = int)

      decim = 16000//sr

      print("decim = {}".format(decim))

      for i in range(0, data.shape[0]):
          p = data["phoneme"][i]    
          if(not (p in dic.keys())):
              dic[p] = count
              count += 1
          for k in range(data["begin"][i]//decim, data["end"][i]//decim):
              #print(k)
              u[k] = dic[p]

      return x, u, dic

  paths = [
      "../data/TIMIT/TRAIN/",
      "../data/TIMIT/TEST/"
  ]

  sr = 8000

  # find files
  path_list = {}

  for path in paths:
      path_list[path] = []
      for p in Path(path).rglob("*.WAV"):
          path_list[path].append(os.path.splitext(p.relative_to("../data/TIMIT/"))[0])

  dic = {}

  for path in paths:
      for rel_path in path_list[path]:
          t0 = time.time()

          x, u, dic = readTimit("../data/TIMIT/" + rel_path, sample_rate = sr, dic = dic)

          x = x.astype(float)

          print("encoding {}. {} samples".format(rel_path, x.size))

          decim = round(sr*10e-3) #1 LPC every 20 miliseconds
          input_size = 10
          K = 1

          mu = 0.5/K#0.9
          x_enc, err, y = nlms_encode(x, input_size, mu, decim, K)

          try:
              print("creating: " + "../out/" + os.path.split(rel_path)[0])
              os.makedirs("../out/" + os.path.split(rel_path)[0])
          except OSError as error:
              print(error)

          ##majority voting
          for i in range(0, (u.size - input_size - 2 - (K - 1)), decim):
              u[i] = np.argmax(np.bincount(u[i:(i + decim)]))

          u = u[0:(x_enc.shape[0]*decim):decim]

          # short time power
          xPower = np.zeros(x_enc.shape[0])
          for i in range(xPower.size):
              xPower[i] = np.mean(x[(i*decim):((i + 1)*decim)]**2)

          xPower /= np.max(xPower)# normalize by peak power

          x_enc = np.concatenate((x_enc, np.reshape(xPower, (xPower.size, 1))), axis = 1)
          np.save("../out/" + rel_path + ".data", x_enc)

          #np.save("../out/" + rel_path + ".phn", u[inds])
          np.save("../out/" + rel_path + ".phn", u)

          print("elapsed: {}s".format(time.time() - t0))

  np.save("../out/dic", dic)

  fig, axis = plt.subplots(3)
  axis[0].plot(x)
  axis[1].plot(xPower)
  axis[2].plot(u)

  plt.show()
#+END_SRC
\pagebreak

#+ATTR_LATEX: :float nil :options fontsize=\small, breaklines, breakautoindent=true
#+BEGIN_SRC c++
// Código da codificação LPC usando algorítimo NLMS (implementado em C++
// para ser utilizado no código em Python utilizando a biblioteca pybind11).
#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
namespace py = pybind11;

#include <utility>
using namespace Eigen;

using T = double;

using Vect = Matrix<T, 1, Dynamic, RowMajor>;
using Matr = Matrix<T, Dynamic, Dynamic, RowMajor>;

std::tuple<Matr, Vect, Vect> nlmsEncode(Ref<Vect> u, size_t n_w, T mu, size_t decim = 5, size_t K = 3, T eps = 1e-4) {
  //encoded vector: n LPC coefficients
  Matr u_enc = Matr::Zero((u.cols() - n_w - 2 - 1 - (K - 1))/decim + 1, n_w);

  Vect w = Vect::Zero(n_w);
  w(n_w - 1) = 1.0f;
    
  Vect e = Vect::Zero(u.cols() - n_w - 2 - (K - 1));
  Vect y = Vect::Zero(u.cols() - n_w - 2 - (K - 1));
    
  for(size_t i = 0; i < u.cols() - n_w - 2 - (K - 1); i++) {
    for(size_t k = 0; k < K; k++) {
      Vect ui = u.segment(i + k, n_w);
      y(i) = w.dot(ui);
      e(i) = u(i + n_w + 1 + k) - y(i);
      w += mu*e(i)*ui/(ui.dot(ui) + eps);
    }
    if(i%decim == 0)
      u_enc.row(i/decim).segment(0, n_w) = w;
  }
  return {u_enc, e, y};
}

PYBIND11_MODULE(nlms_encode, m) {
    m.doc() = "LPC encode signal using NLMS algorithm"; // optional module docstring

    m.def("nlms_encode", &nlmsEncode, "NLMS-LPC encode.",
	  py::arg("u"), py::arg("n_w"), py::arg("mu"),
	  py::arg("decim") = 5, py::arg("K") = 3, py::arg("eps") = 1e-4);
}
#+END_SRC
\pagebreak
#+ATTR_LATEX: :float nil :options fontsize=\small, breaklines, breakautoindent=true
#+BEGIN_SRC python
  ### Código do pré-processamento final (equivalências fonéicas,
  ### balanceamento de classes e redimensionamento dos tensores).

  def read(path, extension = ".npy"):
      dic = np.load("../out/dic.npy", allow_pickle = True).item()

      path_list = []
      for p in Path(path).rglob("*/*.data.npy"):
          path_list.append(os.path.splitext(os.path.splitext(p.relative_to(path))[0])[0])

      u_enc = np.zeros((0, 0))
      y_u = np.zeros((0,))

      for rel_path in path_list:
          x = np.load(path + rel_path + ".data" + extension, allow_pickle = True)
          ci = np.load(path + rel_path + ".phn" + extension, allow_pickle = True)

          u_enc = np.reshape(u_enc, (u_enc.shape[0], x.shape[1]))

          u_enc = np.concatenate((u_enc, x), axis = 0)
          # print("{}, {}".format(x.shape, ci.shape))
          y_u = np.concatenate((y_u, ci))

      print("input data shape: {}".format(u_enc.shape))
      return u_enc, y_u, dic

  plt.close("all")
  path_train = "../out/TRAIN/"
  path_test = "../out/TEST/DR1/"

  # y_u = utils.to_categorical(y_u)
  u_enc, y_u, dic = read(path_train)
  u_enc_test, y_u_test, dic_test = read(path_test)

  #invert dictionary
  dic_inv = {x: y for y, x in dic.items()}

  classes = list(dic.keys())

  #equivalencies
  y_u[y_u == dic["pau"]] = dic["h#"]
  classes.remove("pau")
  y_u[y_u == dic["epi"]] = dic["h#"]
  classes.remove("epi")

  # re-encode
  y_u = np.vectorize(dic_inv.get)(y_u)
  y_u_test = np.vectorize(dic_inv.get)(y_u_test)
  dic_inv = dict(enumerate(classes))
  dic = {x: y for y, x in dic_inv.items()}
  y_u = np.vectorize(dic.get)(y_u)
  y_u_test = np.vectorize(dic.get)(y_u_test)

  # imbalance compensation: calculate class weights
  class_weights = sklearn.utils.class_weight.compute_class_weight("balanced", np.vectorize(dic.get)(classes), y_u)
  class_weights /= np.mean(class_weights)

  # convert data containers to pytoch tensors
  y_u = torch.tensor(y_u)
  y_u_test = torch.tensor(y_u_test)
  u_enc = torch.tensor(u_enc).float()
  u_enc_test = torch.tensor(u_enc_test).float()
  class_weights = torch.tensor(class_weights).float()

  use_gpu = True #for debugging

  gpu_aval = torch.cuda.is_available()
  cpu = torch.device("cpu")
  device = torch.device("cuda") if (gpu_aval and use_gpu) else torch.device("cpu")

  if gpu_aval:
      #y_u = y_u.to(device)
      #y_u_test = y_u_test.to(device)
      #u_enc = u_enc.to(device)
      #u_enc_test = u_enc_test.to(device)
      class_weights = class_weights.to(device)

  num_classes = len(classes)

  #size of phoneme sequences
  seq_size = 16
  #pad sequence to a multiple of seq_size
  u_enc = nn.functional.pad(u_enc, (0, 0, ((u_enc.shape[0] + seq_size - 1)//seq_size)*seq_size - u_enc.shape[0], 0))
  y_u = nn.functional.pad(y_u, (((y_u.shape[0] + seq_size - 1)//seq_size)*seq_size - y_u.shape[0], 0))
  #reshape inputs for LSTM (group in sequences)
  u_enc = u_enc.view(seq_size, u_enc.shape[0]//seq_size, u_enc.shape[1]).transpose(0, 1)
  y_u = y_u.view(seq_size, y_u.shape[0]//seq_size).transpose(0, 1)

  u_enc_test = nn.functional.pad(u_enc_test, (0, 0, ((u_enc_test.shape[0] + seq_size - 1)//seq_size)*seq_size - u_enc_test.shape[0], 0))
  y_u_test = nn.functional.pad(y_u_test, (((y_u_test.shape[0] + seq_size - 1)//seq_size)*seq_size - y_u_test.shape[0], 0))
  u_enc_test = u_enc_test.view(seq_size, u_enc_test.shape[0]//seq_size, u_enc_test.shape[1]).transpose(0, 1)
  y_u_test = y_u_test.view(seq_size, y_u_test.shape[0]//seq_size).transpose(0, 1)
#+END_SRC

* Emacs setup                                                      :noexport:
# Local Variables:
# eval: (add-to-list 'load-path ".")
# eval: (require 'ox-extra)
# eval: (ox-extras-activate '(ignore-headlines))
# eval: (require 'org-ref)
# eval: (require 'doi-utils)
# eval: (setq org-latex-pdf-process (list "latexmk -pdf %f"))
# eval: (add-to-list 'org-export-before-processing-hook (lambda (be) (org-babel-tangle)))
# End:
* Bib file is here                                                 :noexport:

Tangle this file with C-c C-v t

#+begin_src bibtex :tangle refs.bib
                @Comment Adaptive Frequency Tracking
                @ARTICLE{lopes10,
                  author={C. G. {Lopes} and E. H. {Satorius} and P. {Estabrook} and A. H. {Sayed}},
                  journal={IEEE Transactions on Aerospace and Electronic Systems}, 
                  title={Adaptive Carrier Tracking for Mars to Earth Communications During Entry, Descent, and Landing}, 
                  year={2010},
                  volume={46},
                  number={4},
                  pages={1865-1879},
                  doi={10.1109/TAES.2010.5595600}}

              @article{shaugh1988,
                title={Linear predictive coding},
                author={O'Shaughnessy, Douglas},
                journal={IEEE potentials},
                volume={7},
                number={1},
                pages={29--32},
                year={1988},
                publisher={IEEE}
              }

            @INPROCEEDINGS{thasleema2007,
              author={Thasleema, T.M. and Kabeer, V. and Narayanan, N.K.},
              booktitle={International Conference on Computational Intelligence and Multimedia Applications (ICCIMA 2007)}, 
              title={Malayalam Vowel Recognition Based on Linear Predictive Coding Parameters and k-NN Algorithm}, 
              year={2007},
              volume={2},
              number={},
              pages={361-365},
              doi={10.1109/ICCIMA.2007.372}}

          @INPROCEEDINGS{hai2003,
            author={Jiang Hai and Er Meng Joo},
            booktitle={Fourth International Conference on Information, Communications and Signal Processing, 2003 and the Fourth Pacific Rim Conference on Multimedia. Proceedings of the 2003 Joint}, 
            title={Improved linear predictive coding method for speech recognition}, 
            year={2003},
            volume={3},
            number={},
            pages={1614-1618 vol.3},
            doi={10.1109/ICICS.2003.1292740}}

        @article{vieira2016,
          title={Avaliaç{\~a}o de Desempenho na Classificaç{\~a}o de Patologias Lar{\'i}ngeas por An{\'a}lise LPC de Sinais de Voz e Redes Neurais MLP},
          author={Vin{\'i}cius J. D. Vieira and S. Costa and W. C. A. Costa and S. Correia and J. M. F. R. Ara{\'u}jo},
          journal={ChemBioChem},
          year={2016},
          pages={1-6}
        }

      @INPROCEEDINGS{kumar2009,
        author={Paul, Anup Kumar and Das, Dipankar and Kamal, Md. Mustafa},
        booktitle={2009 Seventh International Conference on Advances in Pattern Recognition}, 
        title={Bangla Speech Recognition System Using LPC and ANN}, 
        year={2009},
        volume={},
        number={},
        pages={171-174},
        doi={10.1109/ICAPR.2009.80}}

    @INPROCEEDINGS{sunny2012,
      author={Sunny, Sonia and Peter S., David and Jacob, K. Poulose},
      booktitle={2012 International Conference on Advances in Computing and Communications}, 
      title={Feature Extraction Methods Based on Linear Predictive Coding and Wavelet Packet Decomposition for Recognizing Spoken Words in Malayalam}, 
      year={2012},
      volume={},
      number={},
      pages={27-30},
      doi={10.1109/ICACC.2012.7}}

  @data{timit,
  author = {Garofolo, John and Lamel, Lori and Fisher, William and Fiscus, Jonathan and Pallett, David and Dahlgren, Nancy and Zue, Victor},
  publisher = {Abacus Data Network},
  title = {{TIMIT Acoustic-Phonetic Continuous Speech Corpus}},
  year = {1993},
  version = {V1},
  doi = {11272.1/AB2/SWVENO},
  url = {https://hdl.handle.net/11272.1/AB2/SWVENO}
  }


#+end_src

* Footnotes

[fn:1] The distinction between phonemes and phones is not made explicit here as
most datasets give phonemic (language-dependent) labels for the
signals, even though they are referred to as phonetic labels. This ends up
meaning that many distinct phones, which have a strong
correlation to their LPCs' shapes, will be grouped together in the same
class, which increases the overall complexity of the intermediary
layers of the classifier.
