# -*- coding: utf-8 -*-
# -*- mode: org -*-

#+TITLE: Proposal: Adaptive Linear Predictor-Based Time-Varying Autoregressive Representation of Speech Signals Applied to Recognition Problems
#+AUTHOR: João Pedro de Omena Simas

#+STARTUP: overview indent
#+LANGUAGE: en-us
#+OPTIONS: H:3 creator:nil timestamp:nil skip:nil toc:nil num:t ^:nil ~:~
#+OPTIONS: author:nil title:nil date:nil
#+TAGS: noexport(n) deprecated(d) ignore(i)
#+EXPORT_SELECT_TAGS: export
#+EXPORT_EXCLUDE_TAGS: noexport

#+LATEX_CLASS: IEEEtran
#+LATEX_CLASS_OPTIONS: [conference,letter,10pt,final]
#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER: \usepackage[T1]{fontenc}

* IEEETran configuration for org export + ignore tag (Start Here)  :noexport:

#+begin_src emacs-lisp :results output :session :exports both
(add-to-list 'load-path ".")
(require 'ox-extra)
(ox-extras-activate '(ignore-headlines))
(add-to-list 'org-latex-classes
             '("IEEEtran"
               "\\documentclass{IEEEtran}"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
#+end_src

#+RESULTS:

* Notes :noexport:
  - Theme
  - Goals
  - Methodology
  - Expected Results (?)
  - References

* *The Paper*                                                       :ignore:
** Latex configurations                                             :ignore:
** Front Page                                                       :ignore:
#+BEGIN_EXPORT latex
\makeatletter
\let\orgtitle\@title
\makeatother

\title{\orgtitle}

\author{
\IEEEauthorblockN{
João Pedro de Omena Simas\IEEEauthorrefmark{1}}

\IEEEauthorblockA{\IEEEauthorrefmark{1}
Universidade de São Paulo, São Paulo, Brazil}
}
#+END_EXPORT

#+LaTeX: \maketitle


** Introduction

  Linear predictive coding cite:shaugh1988 is very well know method encoding and
  analyzing speech signals due to their piecewise-stationary
  behavior. This representation is particularly suited to recognition
  of this type of signal because contrary to the original signal
  itself it removes the time shift and amplitude/initial condition
  factor from the signal itself still providing a full model of the
  spectral characteristics of the signal while not needing as many
  coefficients as a DFT while still successfully describing the
  stationary signal.

  Estimating this representation at each new signal block,
  however, can be very costly and noise-sensitive, which can make it
  not feasible for online applications. One solution is to estimate
  the auto-regressive coefficients using an adaptive linear
  predictor. This application is used in telecommunications
  applications, particularly in frequency/carrier tracking
  cite:lopes10. In addition, this approach imposes an limit 
  to the rate with which the LPC can change, which is useful as there's
  a certain limitation on how fast the transitions between phones
  can be, for example, a diphone doesn't jump from one
  vowel phone to the other instantaneously, but in a more "continuous"
  manner.

  In this work, the main goal is to employ this technique for feature
  extraction and classification in speech in applications, mainly
  phone recognition.
  
** Goals
- Evaluate the usability of this adaptive autoregressive model
  representation on MLP-based phone/phoneme[fn:1] recognition applications
  comparing it with exiting methods in the literature.

- If the first step yields good results, check if this method can be
  successfully be used with unsupervised learning approaches, such as
  an autoencoder, to extract meaningful features from the original
  signal in order to do phonetic classification.

- Validate this approach by testing it with existing publicly
  available phonetically labeled corpus, such as the TIMIT cite:timit
  dataset.

** Learning Structure and Methodology 
:PROPERTIES:
:CUSTOM_ID: sec.context
:END:

In the proposed approach, an adaptive linear predictor, in this case based on
an NLMS adaptive filter, is updated with the input signal and it's
coefficients are fed to an ANN to be trained.

Two topologies will be tested. The initial approach is to do
supervised learning on an phonetically labeled corpus, particularly,
speech files from the TIMIT dataset that come with phonetic labeling.

#+CAPTION: Diagram of the direct MLP classifier.
[[../img/diag1.png]]

An second method is to do unsupervised feature learning first, using
either a simple 3-layer autoencoder or a "deep" one to reduce the
dimensionality of the representation, before classification.

#+CAPTION: Diagram of the autoencoder feature learning classifier.
[[../img/diag2.png]]

The simple approach is particularly interesting
not only due to it's lower complexity, but also due to the fact
that there can be some degree of interpretability to it's final
structure. For example, if the encoder is made by an MLP layer with
softmax activation that is properly trained to approximately select one
of it's outputs and the encoder uses a linear (identity)
activation, the overall effect is that the encoder selects the vector
from a list/basis of vectors that is closest to approximate the LPC of
the signal and the encoded vector is a representation of the signal on
this basis, the decoder then just reverts this basis change to bring
the vector back to the original/"canonical" basis, therefore acting much like an
neural MLP-based KNN. In this way, the resulting "basis" vectors,
represented by the neural weights of the second layer would
represent the LPCs of the learned phones/phonemes themselves that can
be easily reconstructed, making the whole structure interpretable. The
feasibility of arriving to such state, however, needs to be
investigated further.

In the case more complex/deep auto-encoder, it's output will have to be
fed into some sort of classifier, MLP-based or not, in order to
translate the state into a phoneme class.

It is important noting that this structure produces an signal of
"phonetic class" versus time and not a transcription of the signal. For
this, some sort of detection/decision would have to be done to
effectively decide the beginning and end of each phone, however, at
least for now, the focus will be only on producing this phonetic value
signal and using the segmentation information given in the dataset
together with majority voting to evaluate the quality of the
classification.

** Related Work and Motivation
:PROPERTIES:
:CUSTOM_ID: sec.relatedwork
:END:

Regarding the use of LPC and variations on speech recognition problems
some work already exists such as cite:thasleema2007, cite:hai2003,
cite:vieira2016, cite:kumar2009.
The unsupervised feature extraction using LPC and ANN has been
discussed in cite:sunny2012.

Finally, the use of the autoregressive model/LPC obtained using an
adaptive filter is discussed in cite:lopes10. Even though it is in a
different context, mainly that of estimating carrier frequency, the
local piecewise stationarity/quasi-stationarity structure is the same as in
speech signals for many modulations, for example, for any FSK-type
modulation or, more generally, any modulation whose symbols are locally
stationary, like, for example, OFDM.

** References                                                        :ignore:

[[bibliographystyle:IEEEtran]]
[[bibliography:refs.bib]]

* Emacs setup                                                      :noexport:
# Local Variables:
# eval: (add-to-list 'load-path ".")
# eval: (require 'ox-extra)
# eval: (ox-extras-activate '(ignore-headlines))
# eval: (require 'org-ref)
# eval: (require 'doi-utils)
# eval: (setq org-latex-pdf-process (list "latexmk -pdf %f"))
# eval: (add-to-list 'org-export-before-processing-hook (lambda (be) (org-babel-tangle)))
# End:
* Bib file is here                                                 :noexport:

Tangle this file with C-c C-v t

#+begin_src bibtex :tangle refs.bib
                @Comment Adaptive Frequency Tracking
                @ARTICLE{lopes10,
                  author={C. G. {Lopes} and E. H. {Satorius} and P. {Estabrook} and A. H. {Sayed}},
                  journal={IEEE Transactions on Aerospace and Electronic Systems}, 
                  title={Adaptive Carrier Tracking for Mars to Earth Communications During Entry, Descent, and Landing}, 
                  year={2010},
                  volume={46},
                  number={4},
                  pages={1865-1879},
                  doi={10.1109/TAES.2010.5595600}}

              @article{shaugh1988,
                title={Linear predictive coding},
                author={O'Shaughnessy, Douglas},
                journal={IEEE potentials},
                volume={7},
                number={1},
                pages={29--32},
                year={1988},
                publisher={IEEE}
              }

            @INPROCEEDINGS{thasleema2007,
              author={Thasleema, T.M. and Kabeer, V. and Narayanan, N.K.},
              booktitle={International Conference on Computational Intelligence and Multimedia Applications (ICCIMA 2007)}, 
              title={Malayalam Vowel Recognition Based on Linear Predictive Coding Parameters and k-NN Algorithm}, 
              year={2007},
              volume={2},
              number={},
              pages={361-365},
              doi={10.1109/ICCIMA.2007.372}}

          @INPROCEEDINGS{hai2003,
            author={Jiang Hai and Er Meng Joo},
            booktitle={Fourth International Conference on Information, Communications and Signal Processing, 2003 and the Fourth Pacific Rim Conference on Multimedia. Proceedings of the 2003 Joint}, 
            title={Improved linear predictive coding method for speech recognition}, 
            year={2003},
            volume={3},
            number={},
            pages={1614-1618 vol.3},
            doi={10.1109/ICICS.2003.1292740}}

        @article{vieira2016,
          title={Avaliaç{\~a}o de Desempenho na Classificaç{\~a}o de Patologias Lar{\'i}ngeas por An{\'a}lise LPC de Sinais de Voz e Redes Neurais MLP},
          author={Vin{\'i}cius J. D. Vieira and S. Costa and W. C. A. Costa and S. Correia and J. M. F. R. Ara{\'u}jo},
          journal={ChemBioChem},
          year={2016},
          pages={1-6}
        }

      @INPROCEEDINGS{kumar2009,
        author={Paul, Anup Kumar and Das, Dipankar and Kamal, Md. Mustafa},
        booktitle={2009 Seventh International Conference on Advances in Pattern Recognition}, 
        title={Bangla Speech Recognition System Using LPC and ANN}, 
        year={2009},
        volume={},
        number={},
        pages={171-174},
        doi={10.1109/ICAPR.2009.80}}

    @INPROCEEDINGS{sunny2012,
      author={Sunny, Sonia and Peter S., David and Jacob, K. Poulose},
      booktitle={2012 International Conference on Advances in Computing and Communications}, 
      title={Feature Extraction Methods Based on Linear Predictive Coding and Wavelet Packet Decomposition for Recognizing Spoken Words in Malayalam}, 
      year={2012},
      volume={},
      number={},
      pages={27-30},
      doi={10.1109/ICACC.2012.7}}

  @data{timit,
  author = {Garofolo, John and Lamel, Lori and Fisher, William and Fiscus, Jonathan and Pallett, David and Dahlgren, Nancy and Zue, Victor},
  publisher = {Abacus Data Network},
  title = {{TIMIT Acoustic-Phonetic Continuous Speech Corpus}},
  year = {1993},
  version = {V1},
  doi = {11272.1/AB2/SWVENO},
  url = {https://hdl.handle.net/11272.1/AB2/SWVENO}
  }


#+end_src

* Footnotes

[fn:1] The distinction between phonemes and phones is not made explicit here as
most datasets give phonemic (language-dependent) labels for the
signals, even though they are referred to as phonetic labels. This ends up
meaning that many distinct phones, which have a strong
correlation to their LPCs' shapes, will be grouped together in the same
class, which increases the overall complexity of the intermediary
layers of the classifier.
