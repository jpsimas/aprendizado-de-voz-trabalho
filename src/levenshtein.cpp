#include <pybind11/pybind11.h>
#include <pybind11/stl.h>
namespace py = pybind11;

#include <algorithm>
#include <vector>

template<typename T>
size_t levenshtein(const T &source, const T &target) {
  if (source.size() > target.size()) {
    return levenshtein(target, source);
  }

  const auto min_size = source.size(), max_size = target.size();
  std::vector<size_t> lev_dist(min_size + 1);

  for(size_t i = 0; i <= min_size; ++i) {
    lev_dist[i] = i;
  }

  for(size_t j = 1; j <= max_size; j++) {
    size_t previous_diagonal = lev_dist[0], previous_diagonal_save;
    lev_dist[0]++;
	
    for(size_t i = 1; i <= min_size; i++) {
      previous_diagonal_save = lev_dist[i];
      if (source[i - 1] == target[j - 1]) {
	lev_dist[i] = previous_diagonal;
      } else {
	lev_dist[i] = std::min(std::min(lev_dist[i - 1], lev_dist[i]), previous_diagonal) + 1;
      }
      previous_diagonal = previous_diagonal_save;
    }
  }

  return lev_dist[min_size];
}

PYBIND11_MODULE(levenshteinpp, m) {
    m.doc() = "levenshtein distance"; // optional module docstring

    m.def("levenshtein", &levenshtein<std::vector<int>>, "levenshtein distance",
	  py::arg("source"), py::arg("target"));
}
