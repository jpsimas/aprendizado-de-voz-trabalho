#include <pybind11/pybind11.h>
#include <pybind11/eigen.h>
#include <Eigen/Dense>
namespace py = pybind11;

#include <utility>
using namespace Eigen;

using T = double;

using Vect = Matrix<T, 1, Dynamic, RowMajor>;
using Matr = Matrix<T, Dynamic, Dynamic, RowMajor>;

std::tuple<Matr, Vect, Vect> nlmsEncode(Ref<Vect> u, size_t n_w, T mu, size_t decim = 5, size_t K = 3, T eps = 1e-4) {
  //encoded vector: n LPC coefficients + initial states
  Matr u_enc = Matr::Zero((u.cols() - n_w - 2 - 1 - (K - 1))/decim + 1, n_w/* + n_w */);

  Vect w = Vect::Zero(n_w);
  w(n_w - 1) = 1.0f;
    
  Vect e = Vect::Zero(u.cols() - n_w - 2 - (K - 1));
  Vect y = Vect::Zero(u.cols() - n_w - 2 - (K - 1));
    
  for(size_t i = 0; i < u.cols() - n_w - 2 - (K - 1); i++) {
    for(size_t k = 0; k < K; k++) {
      Vect ui = u.segment(i + k, n_w);
      y(i) = w.dot(ui);
      e(i) = u(i + k + n_w) - y(i);
      w += mu*e(i)*ui/(ui.dot(ui) + eps);
    }
    if(i%decim == 0) {
      u_enc.row(i/decim) = w;
      //u_enc.row(i/decim).segment(n_w, n_w) = u.segment(i, n_w);
    }
  }
  return {u_enc, e, y};
}

Matr lpcEncode(Ref<Vect> u, size_t n_w, size_t decim = 5) {
  //encoded vector: n LPC coefficients + initial states
  Matr u_enc = Matr::Zero((u.cols() - n_w)/decim - 1, n_w);
    
  for(size_t i = 0; i < (u.cols() - n_w)/decim - 1; i++) {
    Vect p = Vect::Zero(n_w);
    Matr R = Matr::Zero(n_w, n_w);
    for(size_t k = 0; k < decim; k++) {
      Vect ui = u.segment(i*decim + k, n_w);
      T d = u(i*decim + k + n_w);
      p += d*ui;
      R += ui.adjoint()*ui;
    }
    
    u_enc.row(i) = R.ldlt().solve(p.adjoint());//use the fact R is positive semidefinite
  }
  return u_enc;
}

PYBIND11_MODULE(nlms_encode, m) {
    m.doc() = "LPC encode signal using NLMS algorithm"; // optional module docstring

    m.def("nlms_encode", &nlmsEncode, "NLMS-LPC encode.",
	  py::arg("u"), py::arg("n_w"), py::arg("mu"),
	  py::arg("decim") = 5, py::arg("K") = 3, py::arg("eps") = 1e-4);
    
    m.def("lpc_encode", &lpcEncode, "LPC encode.",
	  py::arg("u"), py::arg("n_w"),
	  py::arg("decim") = 5);
}
