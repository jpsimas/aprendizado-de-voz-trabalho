import numpy as np
import keras
from keras import layers
from keras import regularizers
from keras import utils
import tensorflow as tf

import librosa
import soundfile as sf

from matplotlib import pyplot as plt 

import sklearn

from pathlib import Path
import glob
import os

import misc

def read(path, extension = ".npy"):
    dic = np.load("../out/dic.npy", allow_pickle = True).item()

    #print(len(dic))
    
    path_list = []
    for p in Path(path).rglob("*/*.data.npy"):
        path_list.append(os.path.splitext(os.path.splitext(p.relative_to(path))[0])[0])
        
    u_enc = np.zeros((0, 0))##FIX THIS
    y_u = np.zeros((0,))

    for rel_path in path_list:
        x = np.load(path + rel_path + ".data" + extension, allow_pickle = True)
        ci = np.load(path + rel_path + ".phn" + extension, allow_pickle = True)

        u_enc = np.reshape(u_enc, (u_enc.shape[0], x.shape[1]))
        
        u_enc = np.concatenate((u_enc, x), axis = 0)
        # print("{}, {}".format(x.shape, ci.shape))
        y_u = np.concatenate((y_u, ci))
        
    return u_enc, y_u, dic

plt.close("all")
path_train = "../out/TRAIN/"
#path_test = "../out/TEST/DR1/"
path_test = "../out/TEST/"
# path = "../out/TIMIT/"
# rel_path = "TRAIN/DR1/FCJF0/"
# fnames = ["SA1", "SA2", "SI1027", "SI1657", "SI648", "SX127", "SX217", "SX307", "SX37", "SX397"]

# path_list_train = []
# path_list_test = []

# for p in Path(path_train).rglob("*.data.npy"):
#     path_list_train.append(os.path.splitext(os.path.splitext(p.relative_to(path_train))[0])[0])

# for p in Path(path_test).rglob("*.data.npy"):
#         path_list_test.append(os.path.splitext(os.path.splitext(p.relative_to(path_test))[0])[0])

# print(path_list_train)
# print(path_list_test)

# u_enc = np.zeros((0, 10))##FIX THIS
# y_u = np.zeros((0,))

# dic_inv = {v: k for k, v in dic.items()}

# path = path_train
# for rel_path in path_list_train:
#     x = np.load(path + rel_path + ".data" + extension, allow_pickle = True)
#     ci = np.load(path + rel_path + ".phn" + extension, allow_pickle = True)

#     u_enc = np.concatenate((u_enc, x), axis = 0)
#     print("{}, {}".format(x.shape, ci.shape))
#     y_u = np.concatenate((y_u, ci))

# y_u = utils.to_categorical(y_u)
u_enc, y_u, dic = read(path_train)
u_enc_test, y_u_test, dic_test = read(path_test)

#invert dictionary
dic_inv = {x: y for y, x in dic.items()}

classes = list(dic.keys())

#equivalencies
y_u[y_u == dic["pau"]] = dic["h#"]
classes.remove("pau")
y_u[y_u == dic["epi"]] = dic["h#"]
classes.remove("epi")
# y_u[y_u == dic["tcl"]] = dic["h#"]
# classes.remove("tcl")
# y_u[y_u == dic["dcl"]] = dic["h#"]
# classes.remove("dcl")
# y_u[y_u == dic["kcl"]] = dic["h#"]
# classes.remove("kcl")
# y_u[y_u == dic["gcl"]] = dic["h#"]
# classes.remove("gcl")
# y_u[y_u == dic["pcl"]] = dic["h#"]
# classes.remove("pcl")
# y_u[y_u == dic["bcl"]] = dic["h#"]
# classes.remove("bcl")
# y_u[y_u == dic["ax-h"]] = dic["h#"]
# classes.remove("ax-h")
# y_u[y_u == dic["en"]] = dic["n"]
# classes.remove("en")
# y_u[y_u == dic["em"]] = dic["m"]
# classes.remove("em")
# y_u[y_u == dic["eng"]] = dic["ng"]
# classes.remove("eng")
# y_u[y_u == dic["el"]] = dic["l"]
# classes.remove("el")

# remove silence
# u_enc = u_enc[y_u != dic["h#"], :]
# y_u = y_u[y_u != dic["h#"]]
# u_enc_test = u_enc_test[y_u_test != dic["h#"], :]
# y_u_test = y_u_test[y_u_test != dic["h#"]]
# classes.remove("h#")

# select only "eh" and "h#"
# for c in classes[:]:
#     if (c != "eh" and c != "aa" and c != "iy" and c != "h#"):
#         y_u[y_u == dic[c]] = dic["h#"]
#         classes.remove(c)

# select a few phonemes
classes_d = {}
for c in classes:
    classes_d[c] = False

# diphones aw, ey, ow, oy

#classes = ["aa", "ae", "ah", "ao", "ax", "axr", "eh", "er", "ih", "ix", "iy", "uh", "uw", "ux"]
#classes = ["aa", "ae", "ah", "ao"]

for c in classes:
    classes_d[c] = True
    
# inds = inds + (y_u == dic[c])
# inds_test = inds_test + (y_u_test == dic[c])
inds = np.vectorize(classes_d.get)(np.vectorize(dic_inv.get)(y_u))
inds_test = np.vectorize(classes_d.get)(np.vectorize(dic_inv.get)(y_u_test))

u_enc = u_enc[inds, :]
y_u = y_u[inds]

u_enc_test = u_enc_test[inds_test, :]
y_u_test = y_u_test[inds_test]

# re-encode
y_u = np.vectorize(dic_inv.get)(y_u)
y_u_test = np.vectorize(dic_inv.get)(y_u_test)
dic_inv = dict(enumerate(classes))
dic = {x: y for y, x in dic_inv.items()}
y_u = np.vectorize(dic.get)(y_u)
y_u_test = np.vectorize(dic.get)(y_u_test)

# imbalance compensation: calculate class weights
class_weights = sklearn.utils.class_weight.compute_class_weight("balanced", np.vectorize(dic.get)(classes), y_u)

# class_weights = np.ones(len(classes))
# class_weights[dic["h#"]] = 1e-7
# class_weights[dic["pau"]] = 0
# class_weights[dic["epi"]] = 0
# class_weights = np.zeros(len(dic.keys()))
# class_weights[dic["aa"]] = 1
# class_weights[dic["ae"]] = 1
# class_weights[dic["s"]] = 1
# class_weights[dic["sh"]] = 1
# class_weights[dic["iy"]] = 1
# class_weights[dic["k"]] = 1
# class_weights[dic["ao"]] = 1
# class_weights[dic["l"]] = 1
# class_weights[dic["n"]] = 1

class_weights /= np.mean(class_weights)

class_weights_dic = {}
for i in range(0, len(classes)):
    class_weights_dic[dic[classes[i]]] = class_weights[i]

# one-hot encode labels
y_u = utils.to_categorical(y_u, num_classes = len(classes))
y_u_test = utils.to_categorical(y_u_test, num_classes = len(classes))

# group data into frames/timesteps
#timesteps = 2
#y_u_split = tf.keras.preprocessing.timeseries_dataset_from_array(y_u, targets = None, sequence_length = timesteps)

#plot input data
# fig0, axs0 = plt.subplots(3)
# for i in range(0, u_enc.shape[1]):
#     axs0[0].plot(u_enc[:, i])

# build model

num_classes = y_u.shape[1]

print("num phones: {}".format(num_classes))

input_size = u_enc.shape[1]
layer_size = num_classes

n_hidden_layers = 0
learning_rate = 1e-3

reuse_model = False
if(os.path.isfile("../out/model") and reuse_model):
    model = keras.models.load_model("../out/model")
else:
    model = keras.Sequential()
    initializer = "orthogonal"
    model.add(layers.Dense(layer_size, input_shape=(input_size,), activation="tanh", kernel_initializer = initializer))

    #model.add(layers.Reshape((input_size, 1), input_shape = (input_size,)))
    
    #model.add(layers.SimpleRNN(layer_size, activation="tanh", kernel_initializer = initializer))
    #model.add(layers.GRU(layer_size, activation="tanh", kernel_initializer = initializer))
    
    for i in range(0, n_hidden_layers):
        model.add(layers.Dense(layer_size, activation="tanh", kernel_initializer = initializer))

    model.add(layers.Dense(num_classes, activation="softmax", kernel_initializer = initializer))
        
    model.compile(optimizer=keras.optimizers.Adam(lr = learning_rate),
                      loss="categorical_crossentropy",
                      metrics = ["accuracy"])

#cross validadation
# k_cv = 10
# n_cv = y_u.shape[0]//k_cv
# inds = list(range(0, y_u.shape[0]))
# inds_cv = []
# for i in range(0, n_cv):
#     n = np.random.randint(0, len(inds))
#     inds_cv.append(inds[n])
#     inds.pop(n)

# model.fit(u_enc[inds, :], y_u[inds],
#           epochs = 300,
#           shuffle = True,
#           validation_data = (u_enc[inds_cv, :], y_u[inds_cv]))
 
#cb = keras.callbacks.EarlyStopping(monitor="val_acc", mode="max", verbose=3, patience=1)
#cb = keras.callbacks.EarlyStopping(monitor="val_loss", mode="min", verbose=3, patience=3)
cb = keras.callbacks.EarlyStopping(monitor="loss", mode="min", verbose=3, patience=20)
model.fit(u_enc, y_u,
          epochs = 50,
          shuffle = False,
          validation_data = (u_enc_test, y_u_test),
          callbacks = [cb],
          class_weight = class_weights_dic)

#store model
#model.save("../out/model")

# u_res = model.predict(u_enc[inds_cv, :])
u_res = model.predict(u_enc_test)
# plt.figure()
# plt.plot(np.argmax(u_res, axis = -1))
# plt.plot(np.argmax(y_u_test, axis = -1))

phon_det = np.argmax(u_res, axis = -1)
phon_exp = np.argmax(y_u_test, axis = -1)

conf_matr = sklearn.metrics.confusion_matrix(phon_exp, phon_det, labels = np.vectorize(dic.get)(classes), normalize = "true")
labels_ipa = np.vectorize(misc.arpa2ipa)(classes)

print(conf_matr)

fig, ax = plt.subplots(1)
im = ax.matshow(conf_matr, vmin = 0, vmax = 1)
plt.colorbar(im)
ax.set_xticks(np.arange(len(classes)))
ax.set_xticklabels(labels_ipa)
ax.set_yticks(np.arange(len(classes)))
ax.set_yticklabels(labels_ipa)

ax.set_xlabel("Detected")
ax.set_ylabel("Expected")

fig, ax = plt.subplots(1)
ax.plot(np.diag(conf_matr))
plt.ylim((0, 1))
ax.set_xticks(np.arange(len(classes)))
ax.set_xticklabels(labels_ipa)
ax.grid()

#encode results in IPA phonemes
phon_det = np.vectorize(misc.arpa2ipa)(np.vectorize(dic_inv.get)(phon_det))
phon_exp = np.vectorize(misc.arpa2ipa)(np.vectorize(dic_inv.get)(phon_exp))
print("detected:")
print(phon_det[:377])
print("expected:")
print(phon_exp[:377])

#encoded_test = model.predict(u_test)
# # decoded_imgs = decoder.predict(encoded_imgs)

#encoded_num = np.apply_along_axis(np.argmax, 1, np.abs(encoded_test))
# encoded_num = np.apply_along_axis(classEnc, 1, encoded_test)

#plt.figure()
#plt.hist(encoded_num)
#axs[1].plot(encoded_num)
#axs[2].plot(x_test)
#for i in range(0, u_test.shape[1]):
    #axs[3].plot(u_test[:, i])
#plt.figure()
#plt.plot(encoded_num)

plt.show()
