import numpy as np
import keras
from keras import layers
from keras import regularizers

import librosa
import soundfile as sf

from matplotlib import pyplot as plt 

def nlmsEnc(u, n_w, mu = 1e-1, eps = 1e-3):
    u_enc = np.zeros((u.size - n_w - 2, n_w))

    w = np.zeros(n_w)
    w[-1] = 1

    e = np.zeros(u.size - n_w - 2)
    y = np.zeros(u.size - n_w - 2)
    
    for i in range(0, u.size - n_w - 2):
        ui = u[i:(i + n_w)]
        y[i] = np.dot(w, ui)
        d = u[i + n_w + 1]
        e[i] = d - y[i]
        w = w + mu*e[i]*ui/(ui.dot(ui) + eps)
        u_enc[i, :] = w
        
    return u_enc, e, y
    
def classEnc(datum):
    datumEnc = 0;
    for k in range(0, datum.size):
        datumEnc *= 2
        if(datum[k] >= 0.5):
            datumEnc += 1;

    return datumEnc
    

path = "../data/"
fnames = ["aeiou"]#["a", "e", "i", "o", "u"]#["test3"]
extension = ".wav"

u = []

for fname in fnames:
    x, sr = librosa.load(path + fname + extension, sr = None)
    u = np.concatenate((u, x))
    print("fs = " + str(sr))

u += 1e-3*np.random.normal(size = u.shape)
    
input_size = 4
u_enc, err, y = nlmsEnc(u, input_size)

sf.write("../out/y_rec.wav", y, sr)

plt.figure()
plt.plot(10*np.log10(err*err))

plt.figure()
plt.plot(u_enc[:, 0], u_enc[:, 1], "x")

fig, axs = plt.subplots(2)
for i in range(0, u_enc.shape[1]):
    axs[0].plot(u_enc[:, i])

axs[1].plot(u)
    
# # # This is the size of our encoded representations
encoding_dim = 2

input_layer = keras.Input(shape=(input_size,))
encoded = layers.Dense(encoding_dim,
                       activation = 'sigmoid',
                       use_bias = False,
                       kernel_initializer='orthogonal'#,
                       #activity_regularizer = regularizers.l2(-1e-4/encoding_dim)
                       )(input_layer)
decoded = layers.Dense(input_size,
                       activation='sigmoid',
                       use_bias = False,
                       kernel_initializer='orthogonal')(encoded)

autoencoder = keras.Model(input_layer, decoded)
encoder = keras.Model(input_layer, encoded)

encoded_input = keras.Input(shape=(encoding_dim,))
decoder_layer = autoencoder.layers[-1]
decoder = keras.Model(encoded_input, decoder_layer(encoded_input))
       
autoencoder.compile(optimizer='adam', loss='mse')

x_test, sr = librosa.load(path + "test1" + extension, sr = None)
print("fs = " + str(sr))
u_test, err_test, y_test = nlmsEnc(x_test, input_size)
#u_test = u_enc
#x_test = x
#err_test = err

fig, axs = plt.subplots(3)
axs[0].plot(10*np.log10(err_test*err_test))

autoencoder.fit(u_enc, u_enc,
                epochs = 2,
                shuffle = True,
                validation_data = (u_test, u_test))

encoded_test = encoder.predict(u_test)
# decoded_imgs = decoder.predict(encoded_imgs)

#encoded_num = np.apply_along_axis(np.argmax, 1, np.abs(encoded_test))
encoded_num = np.apply_along_axis(classEnc, 1, encoded_test)

#plt.figure()
#plt.hist(encoded_num)
axs[1].plot(encoded_num)
axs[2].plot(x_test)
plt.show()


