import numpy as np
import math

import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import torch.nn.init as init
import torch.utils as utils

import librosa
import soundfile as sf

from matplotlib import pyplot as plt 

import sklearn

from pathlib import Path
import glob
import os

import misc

import time

def read(path, extension = ".npy"):
    dic = np.load("../out/dic.npy", allow_pickle = True).item()

    #print(len(dic))
    
    path_list = []
    for p in Path(path).rglob("*/*.data.npy"):
        path_list.append(os.path.splitext(os.path.splitext(p.relative_to(path))[0])[0])
        
    u_enc = np.zeros((0, 0))
    y_u = np.zeros((0,))

    for rel_path in path_list:
        print("loading: {}\r".format(rel_path), end = "")
        x = np.load(path + rel_path + ".data" + extension, allow_pickle = True)
        ci = np.load(path + rel_path + ".phn" + extension, allow_pickle = True)

        u_enc = np.reshape(u_enc, (u_enc.shape[0], x.shape[1]))
        
        u_enc = np.concatenate((u_enc, x), axis = 0)
        # print("{}, {}".format(x.shape, ci.shape))
        y_u = np.concatenate((y_u, ci))

    print()
    print("input data shape: {}".format(u_enc.shape))
    return u_enc, y_u, dic

def to_categorical(x, num_classes):
    y = np.zeros((x.size, num_classes))
    for i in range(0, x.size):
        y[i, x[i]] = 1
    return y

plt.close("all")
path_train = "../out/TRAIN/"
#path_test = "../out/TEST/DR1/"
path_test = "../out/TEST/selected/"
# path = "../out/TIMIT/"
# rel_path = "TRAIN/DR1/FCJF0/"
# fnames = ["SA1", "SA2", "SI1027", "SI1657", "SI648", "SX127", "SX217", "SX307", "SX37", "SX397"]

# path_list_train = []
# path_list_test = []

# for p in Path(path_train).rglob("*.data.npy"):
#     path_list_train.append(os.path.splitext(os.path.splitext(p.relative_to(path_train))[0])[0])

# for p in Path(path_test).rglob("*.data.npy"):
#         path_list_test.append(os.path.splitext(os.path.splitext(p.relative_to(path_test))[0])[0])

# print(path_list_train)
# print(path_list_test)

# u_enc = np.zeros((0, 10))##FIX THIS
# y_u = np.zeros((0,))

# dic_inv = {v: k for k, v in dic.items()}

# path = path_train
# for rel_path in path_list_train:
#     x = np.load(path + rel_path + ".data" + extension, allow_pickle = True)
#     ci = np.load(path + rel_path + ".phn" + extension, allow_pickle = True)

#     u_enc = np.concatenate((u_enc, x), axis = 0)
#     print("{}, {}".format(x.shape, ci.shape))
#     y_u = np.concatenate((y_u, ci))

# y_u = utils.to_categorical(y_u)
u_enc, y_u, dic = read(path_train)
u_enc_test, y_u_test, dic_test = read(path_test)

#invert dictionary
dic_inv = {x: y for y, x in dic.items()}

classes = list(dic.keys())

#equivalencies
y_u[y_u == dic["pau"]] = dic["h#"]
classes.remove("pau")
y_u[y_u == dic["epi"]] = dic["h#"]
classes.remove("epi")
# y_u[y_u == dic["tcl"]] = dic["h#"]
# classes.remove("tcl")
# y_u[y_u == dic["dcl"]] = dic["h#"]
# classes.remove("dcl")
# y_u[y_u == dic["kcl"]] = dic["h#"]
# classes.remove("kcl")
# y_u[y_u == dic["gcl"]] = dic["h#"]
# classes.remove("gcl")
# y_u[y_u == dic["pcl"]] = dic["h#"]
# classes.remove("pcl")
# y_u[y_u == dic["bcl"]] = dic["h#"]
# classes.remove("bcl")

#y_u[y_u == dic["ax-h"]] = dic["ah"]
#classes.remove("ax-h")
# y_u[y_u == dic["ax"]] = dic["ah"]
# classes.remove("ax")

# y_u[y_u == dic["axr"]] = dic["er"]
# classes.remove("axr")

# y_u[y_u == dic["hv"]] = dic["hh"]
# classes.remove("hv")

# y_u[y_u == dic["ix"]] = dic["ih"]
# classes.remove("ix")

# y_u[y_u == dic["el"]] = dic["l"]
# classes.remove("el")

# y_u[y_u == dic["em"]] = dic["m"]
# classes.remove("em")

# y_u[y_u == dic["en"]] = dic["n"]
# classes.remove("en")
# y_u[y_u == dic["nx"]] = dic["n"]
# classes.remove("nx")

# y_u[y_u == dic["eng"]] = dic["ng"]
# classes.remove("eng")

# y_u[y_u == dic["zh"]] = dic["sh"]
# classes.remove("zh")

# y_u[y_u == dic["ux"]] = dic["uw"]
# classes.remove("ux")

# remove silence
# u_enc = u_enc[y_u != dic["h#"], :]
# y_u = y_u[y_u != dic["h#"]]
# u_enc_test = u_enc_test[y_u_test != dic["h#"], :]
# y_u_test = y_u_test[y_u_test != dic["h#"]]
# classes.remove("h#")

# select only "eh" and "h#"
# for c in classes[:]:
#     if (c != "eh" and c != "aa" and c != "iy" and c != "h#"):
#         y_u[y_u == dic[c]] = dic["h#"]
#         classes.remove(c)

# select a few phonemes
classes_d = {}
for c in classes:
    classes_d[c] = False

# diphones aw, ey, ow, oy

#classes = ["aa", "ae", "ah", "ao", "ax", "axr", "eh", "er", "ih", "ix", "iy", "uh", "uw", "ux"]
#classes = ["aa", "ae", "ah", "ao"]

for c in classes:
    classes_d[c] = True
    
# inds = inds + (y_u == dic[c])
# inds_test = inds_test + (y_u_test == dic[c])
inds = np.vectorize(classes_d.get)(np.vectorize(dic_inv.get)(y_u))
inds_test = np.vectorize(classes_d.get)(np.vectorize(dic_inv.get)(y_u_test))

u_enc = u_enc[inds, :]
y_u = y_u[inds]

u_enc_test = u_enc_test[inds_test, :]
y_u_test = y_u_test[inds_test]

# re-encode
y_u = np.vectorize(dic_inv.get)(y_u)
y_u_test = np.vectorize(dic_inv.get)(y_u_test)
dic_inv = dict(enumerate(classes))
dic = {x: y for y, x in dic_inv.items()}
y_u = np.vectorize(dic.get)(y_u)
y_u_test = np.vectorize(dic.get)(y_u_test)

# imbalance compensation: calculate class weights
#class_weights = sklearn.utils.class_weight.compute_class_weight("balanced", np.vectorize(dic.get)(classes), y_u)

class_weights = np.ones(len(classes))
# class_weights[dic["h#"]] = 1e-7
# class_weights[dic["pau"]] = 0
# class_weights[dic["epi"]] = 0
# class_weights = np.zeros(len(dic.keys()))
# class_weights[dic["aa"]] = 1
# class_weights[dic["ae"]] = 1
# class_weights[dic["s"]] = 1
# class_weights[dic["sh"]] = 1
# class_weights[dic["iy"]] = 1
# class_weights[dic["k"]] = 1
# class_weights[dic["ao"]] = 1
# class_weights[dic["l"]] = 1
# class_weights[dic["n"]] = 1

class_weights /= np.mean(class_weights)

# class_weights_dic = {}
# for i in range(0, len(classes)):
#     class_weights_dic[dic[classes[i]]] = class_weights[i]
    
# convert data containers to pytoch tensors
y_u = torch.tensor(y_u)
y_u_test = torch.tensor(y_u_test)
u_enc = torch.tensor(u_enc).float()
u_enc_test = torch.tensor(u_enc_test).float()
class_weights = torch.tensor(class_weights).float()

use_gpu = True #for debugging

gpu_aval = torch.cuda.is_available()
cpu = torch.device("cpu")
device = torch.device("cuda") if (gpu_aval and use_gpu) else torch.device("cpu")

if gpu_aval:
    #y_u = y_u.to(device)
    #y_u_test = y_u_test.to(device)
    #u_enc = u_enc.to(device)
    #u_enc_test = u_enc_test.to(device)
    class_weights = class_weights.to(device)

#plot input data
# fig0, axs0 = plt.subplots(3)
# for i in range(0, u_enc.shape[1]):
#     axs0[0].plot(u_enc[:, i].cpu().numpy())

# build model

num_classes = len(classes)

print("num phones: {}".format(num_classes))

class LinearOrth(nn.Linear):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    def reset_parameters(self) -> None:
        nn.init.orthogonal_(self.weight)
        nn.init.zeros_(self.bias)

class ModelLSTM(nn.Module):
        def __init__(self, input_size, output_size, hidden_layer_size, n_hidden_layers):
            super().__init__()
            
            self.input_size = input_size
            self.hidden_layer_size  = hidden_layer_size
            self.output_size  = output_size

            self.lstm = nn.LSTM(self.input_size, self.hidden_layer_size, n_hidden_layers + 1, bidirectional = True, batch_first = False)
            self.output_layer = nn.Linear(2*self.hidden_layer_size, self.output_size)

            self.softmax = nn.LogSoftmax(dim = 1)
            
        def forward(self, x):
            y, hidden = self.lstm(x)

            return self.softmax(self.output_layer(y))

        
input_size = u_enc.shape[1]

layer_size = 4*num_classes#4*num_classes
output_size = num_classes + 1# + 1 because of CTC blank class
        
n_hidden_layers = 1
learning_rate = 1e-3#1e-3

#model = Model(input_size, output_size, layer_size, n_hidden_layers)
model = ModelLSTM(input_size, output_size, layer_size, n_hidden_layers)

#size of phoneme sequences
seq_size = 256#64#16
stride = seq_size//2

print(u_enc.size())
print(u_enc[0:10, :])
print(y_u[0:30])

#pad sequence to a multiple of seq_size
u_enc = nn.functional.pad(u_enc, (0, 0, 0, ((u_enc.shape[0] + seq_size - 1)//seq_size)*seq_size - u_enc.shape[0]))
y_u = nn.functional.pad(y_u, (0, (((y_u.shape[0] + seq_size - 1)//seq_size)*seq_size - y_u.shape[0])))
#reshape inputs for LSTM (group in sequences)

print(u_enc.size())
print(u_enc[0:10, :])
print(y_u[0:30])

u_enc = u_enc.view(u_enc.shape[0]//seq_size, seq_size, u_enc.shape[1])
y_u = y_u.view(y_u.shape[0]//seq_size, seq_size)

print(u_enc.size())
print(u_enc[0:10, 0, :])

u_enc_test = nn.functional.pad(u_enc_test, (0, 0, 0, ((u_enc_test.shape[0] + seq_size - 1)//seq_size)*seq_size - u_enc_test.shape[0]))
y_u_test = nn.functional.pad(y_u_test, (0, (((y_u_test.shape[0] + seq_size - 1)//seq_size)*seq_size - y_u_test.shape[0])))
u_enc_test = u_enc_test.view(u_enc_test.shape[0]//seq_size, seq_size, u_enc_test.shape[1])
y_u_test = y_u_test.view(y_u_test.shape[0]//seq_size, seq_size)

#add 1 to class labels
y_u += 1
y_u_test += 1

print(y_u.size())
print(y_u[0:10, :])
print(u_enc.size())
print(u_enc[0:10, 0, :])

#remove duplicates from validation set
test_target_lengths = torch.zeros(y_u_test.size()[0], dtype = torch.int32)
test_input_lengths = torch.full((y_u_test.size()[0],), seq_size, dtype = torch.int32)

for k in range(0, y_u_test.size()[0]):
    reduc = torch.unique_consecutive(y_u_test[k, :], dim = 0)#check axis
    y_u_test[k, :] = 0
    test_target_lengths[k] = reduc.size()[0]
    y_u_test[k, 0:(reduc.size()[0])] = reduc

print("using device {}".format(device))

reuse_model = True

if(os.path.isfile("../out/modelCTC") and reuse_model):
    model.load_state_dict(torch.load("../out/modelCTC"))   
    # model.load_state_dict(torch.load("../out/modelEntr"))
    print("Loaded previous model state.")

model.to(device)
model.train()

optimizer = optim.Adam(model.parameters(), lr = learning_rate)
#optimizer = optim.SGD(model.parameters(), lr = learning_rate, momentum = 0.9)

n_iter = u_enc.shape[0]
n_iter_test = u_enc_test.shape[0]

n_epoch = 10#15

batch_size = 32#512//seq_size#512//seq_size
dataset = utils.data.TensorDataset(u_enc, y_u)
loader = torch.utils.data.DataLoader(dataset = dataset,
                                     batch_size = batch_size,
                                     shuffle = False)#, num_workers = 1)

n_batches = len(loader)
train_losses = np.zeros(n_epoch)
val_losses = np.zeros(n_epoch)
loss_test_prev = 1000

early_stop = False

for i in range(0, n_epoch):
    t = time.time()
    print("Running epoch: {}".format(i))
    for step, (u_enc_j, y_u_j) in enumerate(loader):
        optimizer.zero_grad()
        y = model(u_enc_j.transpose(0, 1).to(device))
        lossFunc =  nn.CTCLoss(blank = 0, zero_infinity = True)

        batch_size_j = y_u_j.size()[0]
        
        target_lengths = torch.zeros(batch_size_j, dtype = torch.int32)

        max_target_length = torch.max(target_lengths)
        if(max_target_length > 256):
            print("WARNING: target_length > 256")
        
        # print(y_u_j.size())
        # print(y_u_j)
        
        for k in range(0, batch_size_j):
            reduc = torch.unique_consecutive(y_u_j[k, :], dim = 0)#check axis
            y_u_j[k, :] = 0
            target_lengths[k] = reduc.size()[0]
            y_u_j[k, 0:(reduc.size()[0])] = reduc

        # print(y.size())
        # print(y_u_j.size())
        # print(y_u_j)

        # print(target_lengths)
        # exit()

        input_lengths = torch.full((batch_size_j,), seq_size, dtype = torch.int32)
        # print(target_lengths.size())
        # print(batch_size)

        # print(y.transpose(0, 1))
        # print(y_u_j)
        # print(input_lengths)
        # print(target_lengths)
        
        loss = lossFunc(y, y_u_j.to(device), input_lengths, target_lengths)
        loss.backward()
        optimizer.step()
        print("Batch {} / {} loss: {:.6f}\r".format(step, n_batches, loss.item()), end = "")
        #print()
        #exit()
        
    with torch.no_grad():
        # print()
        y_test = model(u_enc_test.transpose(0, 1).to(device))
        # print(y_test.size())
        # print(y_u_test.size())
        # print(test_input_lengths.size())
        # print(test_target_lengths.size())
        
        loss_test = lossFunc(y_test, y_u_test.to(device), test_input_lengths, test_target_lengths)

        # store losses
        train_losses[i] = loss.item()
        val_losses[i] = loss_test.item()

        if (gpu_aval and use_gpu):
            torch.cuda.synchronize()
            
        t = time.time() - t
        #print("run time : {:.6f} s loss: {:.6f}".format(t, loss.item()))
        print("run time : {:.6f} s loss: {:.6f} val_loss: {:.6f}".format(t, loss.item(), loss_test.item()))

        if((loss_test.item() > loss_test_prev) and early_stop):
            break
        loss_test_prev = loss_test.item()

plt.figure()
plt.plot(train_losses, label = "Training")
plt.plot(val_losses, label = "Validation")
plt.legend()
 
# save model
torch.save(model.state_dict(), "../out/modelCTC")

#transfer model to cpu to compute validation output
model.to(cpu)

# u_res = model.predict(u_enc[inds_cv, :])
#u_res = model.predict(u_enc_test)

u_res = model(u_enc_test.transpose(0, 1))
u_res = u_res.transpose(0, 1).reshape(u_res.shape[0]*u_res.shape[1], u_res.shape[2])

# plt.figure()
# plt.plot(np.argmax(u_res, axis = -1))
# plt.plot(np.argmax(y_u_test, axis = -1))

print(y_u_test.size())
print(y_u_test[0:10, :])

dic['-'] = -1
dic_inv[-1] = '-'

phon_det = torch.argmax(u_res, dim = -1)  #subtract 1 to get to original encoding
#phon_det = phon_det[phon_det != 0] - 1
phon_det -= 1
#phon_det = torch.unique_consecutive(phon_det)

phon_exp = y_u_test.reshape(y_u_test.shape[0]*y_u_test.shape[1]).detach().cpu()
phon_exp = phon_exp[phon_exp != 0] - 1
phon_exp = torch.unique_consecutive(phon_exp)

#acc = torch.sum(phon_det == phon_exp)/phon_det.size
per = misc.calcPER(phon_det, phon_exp)
#print("val acc: {}, PER: {}".format(acc, per))
print("PER: {}".format(per))

#conf_matr = sklearn.metrics.confusion_matrix(phon_exp, phon_det, labels = np.vectorize(dic.get)(classes), normalize = "true")
labels_ipa = np.vectorize(misc.arpa2ipa)(classes)

#print(conf_matr)

# fig, ax = plt.subplots(1)
# im = ax.matshow(conf_matr, vmin = 0, vmax = 1)
# plt.colorbar(im)
# ax.set_xticks(np.arange(len(classes)))
# ax.set_xticklabels(labels_ipa)
# ax.set_yticks(np.arange(len(classes)))
# ax.set_yticklabels(labels_ipa)

# ax.set_xlabel("Detected")
# ax.set_ylabel("Expected")

# fig, ax = plt.subplots(1)
# ax.plot(np.diag(conf_matr))
# plt.ylim((0, 1))
# ax.set_xticks(np.arange(len(classes)))
# ax.set_xticklabels(labels_ipa)
# ax.grid()

# print(np.diag(conf_matr))

print(phon_exp)

#encode results in IPA phonemes
phon_det = np.vectorize(misc.arpa2ipa)(np.vectorize(dic_inv.get)(phon_det.numpy()))
phon_exp = np.vectorize(misc.arpa2ipa)(np.vectorize(dic_inv.get)(phon_exp.numpy()))
print("detected:")
print(phon_det[:377])
print("expected:")
print(phon_exp[:377])

#encoded_test = model.predict(u_test)
# # decoded_imgs = decoder.predict(encoded_imgs)

#encoded_num = np.apply_along_axis(np.argmax, 1, np.abs(encoded_test))
# encoded_num = np.apply_along_axis(classEnc, 1, encoded_test)

#plt.figure()
#plt.hist(encoded_num)
#axs[1].plot(encoded_num)
#axs[2].plot(x_test)
#for i in range(0, u_test.shape[1]):
    #axs[3].plot(u_test[:, i])
#plt.figure()
#plt.plot(encoded_num)

# plt.show()
