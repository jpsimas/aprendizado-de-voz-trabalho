import numpy as np
import keras
from keras import layers
from keras import regularizers
from keras import utils

import librosa
import soundfile as sf

from matplotlib import pyplot as plt 

def nlmsEnc(u, n_w, mu, eps = 1e-3, decim = 5, K = 3):
    print("mu = {}. decim = {}. K = {}".format(mu, decim, K))
    
    u_enc = np.zeros(((u.size - n_w - 2 - 1 - (K - 1))//decim + 1, n_w))

    w = np.random.normal(size = (n_w,))/np.sqrt(n_w)
    
    e = np.zeros(u.size - n_w - 2 - (K - 1))
    y = np.zeros(u.size - n_w - 2 - (K - 1))
    
    for i in range(0, u.size - n_w - 2 - (K - 1)):
        for k in range(0, K):
            ui = u[(i + k):(i + n_w + k)]
            y[i] = np.dot(w, ui)
            d = u[i + n_w + 1 + k]
            e[i] = d - y[i]
            w = w + mu*e[i]*ui/(ui.dot(ui) + eps)
        if(i%decim == 0):
            u_enc[i//decim, :] = w
        
    return u_enc, e, y
    
def classEnc(datum):
    datumEnc = 0;
    for k in range(0, datum.size):
        datumEnc *= 2
        if(datum[k] >= 0.5):
            datumEnc += 1;

    return datumEnc

def LPCdecode(u_enc, decim, initial_state = None):
    nW = u_enc.shape[1]
    if(initial_state == None):
      initial_state = np.zeros(nW)
      initial_state[0] = 1

    u = np.zeros(u_enc.shape[0]*decim + nW)
    u[0:nW] = initial_state
      
    for i in range(0, u_enc.shape[0]):
        for k in range(0, decim):
            u[i*decim + k + nW] = np.dot(u_enc[i, :], u[(i*decim + k):(i*decim + k + nW)])

    return u
            

def main():
    path = "../data/"
    fnames = ["a2", "e2", "i2", "o2", "u2"]#, "o", "u"]#["test3"]
    fname_dic = {"a" : 0, "e" : 1, "i" : 2, "o" : 3, "u" : 4, "a2" : 0, "e2" : 1, "i2" : 2, "o2" : 3, "u2" : 4}
    extension = ".wav"
    sample_rate = 8000

    u = []
    y_u = []

    for fname in fnames:
        x, sr = librosa.load(path + fname + extension, sr = sample_rate)
        u = np.concatenate((u, x))
        y_u = np.concatenate((y_u, np.full(x.shape, fname_dic[fname])))
        print("fs = " + str(sample_rate))

    y_u = utils.to_categorical(y_u)
    
    #u += np.sqrt(1e-6)*np.random.normal(size = u.shape)
    
    input_size = 10
    decim = round(sample_rate*10e-3) #1 LPC every 10 miliseconds
    K = 5
    mu = 1.0/K#0.9
    u_enc, err, y = nlmsEnc(u, input_size, mu = mu, decim = decim, K = K)

    y_dec = LPCdecode(u_enc, decim)
    sf.write("../out/u.wav", u, sr)
    sf.write("../out/y_dec.wav", y_dec, sr)

    print(u_enc[-1, :])

    print(u.shape)
    print(u_enc.shape)
    print(y_u.shape)
    
    y_u = y_u[(input_size + 2):, :]
    print(y_u.shape)
    y_u = y_u[0::decim, :]

    print(y_u.shape)
    
    sf.write("../out/y_rec.wav", y, sr)

    # plt.figure()
    # plt.plot(10*np.log10(err*err))

    # plt.figure()
    # plt.plot(u_enc[:, 0], u_enc[:, 1], "x")

    fig0, axs0 = plt.subplots(3)
    for i in range(0, u_enc.shape[1]):
        axs0[0].plot(u_enc[:, i])

    axs0[1].plot(u)

    num_classes = y_u.shape[1]

    layer_size = 15
    
    model = keras.Sequential()
    model.add(layers.Dense(layer_size, input_shape=(input_size,), activation="relu"))
    model.add(layers.Dense(layer_size, activation="relu"))
    model.add(layers.Dense(layer_size, activation="relu"))
    model.add(layers.Dense(layer_size, activation="relu"))
    model.add(layers.Dense(layer_size, activation="relu"))
    model.add(layers.Dense(layer_size, activation="relu"))
    model.add(layers.Dense(num_classes, activation="softmax"))

    model.compile(optimizer=keras.optimizers.Adam(lr = 1e-2),
                  loss="categorical_crossentropy")

    x_test, sr = librosa.load(path + "aeiou" + extension, sr = sample_rate)
    print("fs = " + str(sr))
    u_test, err_test, y_test = nlmsEnc(x_test, input_size, mu = mu, decim = decim, K = K)
    #u_test = u_enc
    #x_test = x
    #err_test = err

    fig, axs = plt.subplots(4)
    axs[0].plot(10*np.log10(err_test*err_test))
    
    model.fit(u_enc, y_u,
              epochs = 3)
    # shuffle = True,
    # validation_data = (u_test, u_test))
    
    u_res = model.predict(u_enc)
    axs0[2].plot(np.argmax(u_res, axis = -1))
    
    encoded_test = model.predict(u_test)
    # # decoded_imgs = decoder.predict(encoded_imgs)

    encoded_num = np.apply_along_axis(np.argmax, 1, np.abs(encoded_test))
    # encoded_num = np.apply_along_axis(classEnc, 1, encoded_test)

    #plt.figure()
    #plt.hist(encoded_num)
    axs[1].plot(encoded_num)
    axs[2].plot(x_test)
    for i in range(0, u_test.shape[1]):
        axs[3].plot(u_test[:, i])
    #plt.figure()
    #plt.plot(encoded_num)
    plt.show()

if (__name__ == "__main__"):
    main()


