import pandas as pd
import numpy as np
import librosa
import soundfile as sf
import scipy

from matplotlib import pyplot as plt 

from pathlib import Path
import os

import time

from nlms_encode import nlms_encode, lpc_encode

def removeSilenceInds(u, decim = 1, length = None, threshold = 1e-3, alpha = 1 - 1e-3):
    if(length == None):
        length = u.size
    
    uPower = u*u
    uPower = scipy.signal.lfilter([1 - alpha], [1, -alpha], uPower)
    uPower /= np.max(uPower)
    return (uPower[:length:decim] >= threshold), uPower

def readTimit(fname, sample_rate = None, dic = {}):

    count = len(dic)
    
    data = pd.read_csv(fname + ".PHN", delimiter = " ", header = None, names = ["begin", "end", "phoneme"], dtype = {"begin" : int, "end" : int, "phoneme" : str})
    
    x, sr = librosa.load(fname + ".WAV", sample_rate)
    x = librosa.effects.preemphasis(x)
    
    u = np.zeros(x.shape, dtype = int)

    decim = 16000//sr

    print("decim = {}".format(decim))
    
    for i in range(0, data.shape[0]):
        p = data["phoneme"][i]    
        if(not (p in dic.keys())):
            dic[p] = count
            count += 1
        for k in range(data["begin"][i]//decim, data["end"][i]//decim):
            #print(k)
            u[k] = dic[p]

    return x, u, dic

def nlmsEnc(u, n_w, mu, eps = 1e-4, decim = 5, K = 3):
    print("mu = {}. decim = {}. K = {}".format(mu, decim, K))
    
    u_enc = np.zeros(((u.size - n_w - 2 - 1 - (K - 1))//decim + 1, n_w))#, dtype = np.float32)

    #w = np.random.normal(size = (n_w,)).astype(np.float32)/np.sqrt(n_w)
    w = np.random.normal(size = (n_w,))/np.sqrt(n_w)
    w -= np.mean(w)
    
    e = np.zeros(u.size - n_w - 2 - (K - 1))#, dtype = np.float32)
    y = np.zeros(u.size - n_w - 2 - (K - 1))#, dtype = np.float32)

    # uMax = np.max(u*u)
    # print("uMax = {}".format(uMax))
    # mu /= n_w*uMax
    # print("mu = {}".format(mu))
    
    for i in range(0, u.size - n_w - 2 - (K - 1)):
        for k in range(0, K):
            ui = u[(i + k):(i + n_w + k)]
            y[i] = np.dot(w, ui)
            e[i] = u[i + n_w + 1 + k] - y[i]
            w = w + mu*e[i]*ui/(ui.dot(ui) + eps)
        if(i%decim == 0):
            u_enc[i//decim, :] = w
            
    return u_enc, e, y

def LPCDecode(u_enc, decim):
    nW = u_enc.shape[1]

    u = np.zeros(u_enc.shape[0]*decim + nW)#, dtype = np.float32)
    #u[::decim] = 1
    u[0] = 1e-3
    
    print("ENCABULATOR 4 STARTED")
    print(u.shape)

    p = 1
    alpha = 1e-2
    
    for i in range(0, u_enc.shape[0]):
        for k in range(0, decim):
            u[i*decim + k + nW] = np.dot(u_enc[i, :], u[(i*decim + k):(i*decim + k + nW)])#/np.sqrt(p + 1e-6)
            #p = alpha*u[i*decim + k + nW]*u[i*decim + k + nW] + (1 - alpha)*p

    u /= np.sqrt(np.max(u*u)) + 1e-6
            
    return u

#paths = ["../data/TIMIT/TRAIN/DR1/",
         #"../data/TIMIT/TEST/DR1/"]

paths = [
    #"../data/TIMIT/TRAIN/DR1/FCJF0",
    # "../data/TIMIT/TRAIN/DR1/FDAW0",
    # "../data/TIMIT/TRAIN/DR1/FDML0",
    # "../data/TIMIT/TRAIN/DR1/FECD0",
    # "../data/TIMIT/TRAIN/DR1/FETB0",
    # "../data/TIMIT/TRAIN/DR1/FETB0",
    # "../data/TIMIT/TRAIN/DR1/MCPM0",
    # "../data/TIMIT/TEST/DR1/FAKS0",
    "../data/TIMIT/TRAIN/",
    #"../data/TIMIT/TEST/DR1/"
    "../data/TIMIT/TEST/"
]

sr = 8000

# find files
path_list = {}

for path in paths:
    path_list[path] = []
    for p in Path(path).rglob("*.WAV"):
        path_list[path].append(os.path.splitext(p.relative_to("../data/TIMIT/"))[0])

dic = {}

for path in paths:
    for rel_path in path_list[path]:
        t0 = time.time()
        
        x, u, dic = readTimit("../data/TIMIT/" + rel_path, sample_rate = sr, dic = dic)

        x = x.astype(float)
        
        print("encoding {}. {} samples".format(rel_path, x.size))

        decim = round(sr*10e-3) #1 LPC every 20 miliseconds
        input_size = 10
        K = 1
        
        # inds, xPower = removeSilenceInds(x, decim = decim, length = (u.size - input_size - 2 - (K - 1)))
    
        mu = 0.5/K#0.9
        #x_enc, err, y = nlmsEnc(x, input_size, mu = mu, decim = decim, K = K)
        #x_init = np.zeros(input_size)
        #x_init[-1] = 1
        #x = np.concatenate((x_init, x)) # Pad with zeros
        #x_enc, err, y = nlms_encode(x, input_size, mu, decim, K)
        x_enc = lpc_encode(x, input_size, decim)
    
        try:
            print("creating: " + "../out/" + os.path.split(rel_path)[0])
            os.makedirs("../out/" + os.path.split(rel_path)[0])
        except OSError as error:
            print(error)
    
        #np.save("../out/" + rel_path + ".data", x_enc[inds, :])
        
        ##majority voting
        for i in range(0, (u.size - input_size - 2 - (K - 1)), decim):
            u[i] = np.argmax(np.bincount(u[i:(i + decim)]))
        #u = u[0:(u.size - input_size - 2 - (K - 1)):decim]
        u = u[0:(x_enc.shape[0]*decim):decim]

        # short time power
        xPower = np.zeros(x_enc.shape[0])
        for i in range(xPower.size):
            xPower[i] = np.mean(x[(i*decim):((i + 1)*decim)]**2)

        xPower /= np.max(xPower)# normalize by peak power
            
        x_enc = np.concatenate((x_enc, np.reshape(xPower, (xPower.size, 1))), axis = 1)
        np.save("../out/" + rel_path + ".data", x_enc)
        
        #np.save("../out/" + rel_path + ".phn", u[inds])
        np.save("../out/" + rel_path + ".phn", u)

        print("elapsed: {}s".format(time.time() - t0))
    
np.save("../out/dic", dic)
# fig, axis = plt.subplots(2)
# axis[0].plot(x[inds])
# axis[1].plot(u[inds])

# sf.write("../out/ySilRem.wav", x[inds], sr)

fig, axis = plt.subplots(3)
axis[0].plot(x)
axis[1].plot(xPower)
axis[2].plot(u)

# fig, ax = plt.subplots(3)

# for i in range(0, x_enc.shape[1]):
#     ax[0].plot(x_enc[:, i])

# xDec = LPCDecode(x_enc, decim)
# ax[1].plot(np.log10(np.abs(xDec)))

# ax[2].plot(xDec)
# ax[2].plot(x)

# sf.write("../out/x_dec.wav", xDec.astype(np.float32), sr)

plt.show()
