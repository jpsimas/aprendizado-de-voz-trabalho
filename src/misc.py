import numpy as np
from levenshteinpp import levenshtein

def arpa2ipa(x):
    dic = {"aa" : "ɑ",
           "ae" : "æ",
           "ah" : "ʌ",
           "ao" : "ɔ",
           "aw" : "aʊ",
           "ax" : "ə",
           "axr" : "ɚ",
           "ay" : "aɪ",
           "eh" : "ɛ",
           "er" : "ɝ",
           "ey" : "eɪ",
           "ih" : "ɪ",
           "ix" : "ɨ",
           "iy" : "i",
           "ow" : "oʊ",
           "oy" : "ɔɪ",
           "uh" : "ʊ",
           "uw" : "u",
           "ux" : "ʉ",
           "b" : "b",
           "ch" : "tʃ",
           "d" : "d",
           "dh" : "ð",
           "dx" : "ɾ",
           "el" : "l̩",
           "em" : "m̩",
           "en" : "n̩",
           "f" : "f",
           "g" : "ɡ",
           "hh" : "h",
           "jh" : "dʒ",
           "k" : "k",
           "l" : "l",
           "m" : "m",
           "n" : "n",
           "ng" : "ŋ",
           "nx" : "ɾ̃",
           "p" : "p",
           "q" : "ʔ",
           "r" : "ɹ",
           "s" : "s",
           "sh" : "ʃ",
           "t" : "t",
           "th" : "θ",
           "v" : "v",
           "w" : "w",
           "wh" : "ʍ",
           "y" : "j",
           "z" : "z",
           "zh" : "ʒ",
           "ax-h" : "ə̥",
           "bcl" : "b̚",
           "dcl" : "d̚",
           "eng" : "ŋ̍",
           "gcl" : "ɡ̚",
           "hv" : "ɦ",
           "kcl" : "k̚",
           "pcl" : "p̚",
           "tcl" : "t̚",
           "pau" : "pau",
           "epi" : "epi",
           "h#" : "h#"}
    return dic[x] if x in dic.keys() else x

def remDuplicates(x):
    y = []
    y.append(x[0])
    for i in range(1, len(x)):
        if(x[i] != y[-1]):
            y.append(x[i])
    return y

def calcPER(pred, exp):
    
    pred_rem = remDuplicates(pred)
    exp_rem = remDuplicates(exp)

    return levenshtein(pred_rem, exp_rem)/len(exp_rem)
